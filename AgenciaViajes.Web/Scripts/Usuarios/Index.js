﻿$(document).ready(function () {
    $('#btnAgregar').click(function (e) {
        e.preventDefault();
        window.location = "/Usuarios/Agregar?id=" + $('#txtIdUsuario').val() + "&name=" + $('#txtNombre').val() + "&username=" + $('#txtUsername').val() + "&password=" + $('#txtContrasena').val() + "&tipoUsuario=" + $('#txtTipoUsuario').val();
    });
});

function eliminar(id) {
    window.location = "/Usuarios/Eliminar?id=" + id;
}

function actualizarUsuario(id) {
    window.location = "/Usuarios/ActualizarUsuario?id=" + id;
}