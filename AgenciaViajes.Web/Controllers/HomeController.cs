﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgenciaViajes.Web.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index(string username, string password)
        {
            ClientesService.ClientesServerClient client = new ClientesService.ClientesServerClient();
            var list = client.getAllUsuarios();

            if (!String.IsNullOrEmpty(username))
            {
                var res = client.login(username, password);

                if (res != null)
                {
                    ViewBag.InicioSesion = "true";
                    ViewBag.usuarioId = res.id;
                    ViewBag.tipoUsuario = res.tipoUsuario.ToString();
                    ViewBag.username = res.username;
                    return View();
                }
                else
                {
                    return RedirectToAction("Login", "Usuarios");
                }
            }
            ViewBag.InicioSesion = "false";
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}