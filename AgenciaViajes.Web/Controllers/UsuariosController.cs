﻿using AgenciaViajes.Web.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgenciaViajes.Web.Controllers
{
    public class UsuariosController : Controller
    {
        public ActionResult ActualizarPerfil(int idUsuario)
        {
            ClientesService.ClientesServerClient client = new ClientesService.ClientesServerClient();
            var res = client.getUsuario(idUsuario);

            if (res != null)
            {
                Usuario usuario = new Usuario()
                {
                    id = res.id,
                    username = res.username,
                    nombre = res.nombre,
                    contrasena = res.contrasena,
                    tipoUsuario = res.tipoUsuario
                };
                return View(usuario);
            }
            else
            {
                return RedirectToAction("Home", "Usuarios");
            }
        }

        public ActionResult VerPerfil(int idUsuario, string nombre, string username, string contrasena, string tipoUsuario)
        {
            ClientesService.ClientesServerClient client = new ClientesService.ClientesServerClient();

            if (!String.IsNullOrEmpty(nombre))
            {
                client.updateUsuario(idUsuario, nombre, contrasena, tipoUsuario, username);
            }

            var res = client.getUsuario(idUsuario);

            if (res != null)
            {
                Usuario usuario = new Usuario()
                {
                    id = res.id,
                    username = res.username,
                    nombre = res.nombre,
                    contrasena = res.contrasena,
                    tipoUsuario = res.tipoUsuario
                };
                return View(usuario);
            }
            else
            {
                return RedirectToAction("Home", "Usuarios");
            }
        }

        public ActionResult Login(string id, string name, string password, string username)
        {
            if (!String.IsNullOrEmpty(name))
            {
                ClientesService.ClientesServerClient client = new ClientesService.ClientesServerClient();
                client.register(int.Parse(id), name, password, username);
            }
            return View();
        }

        public ActionResult Register()
        {
            return View();
        }

        public ActionResult AgregarUsuario()
        {
            return View();
        }

        public ActionResult ActualizarUsuario(int id)
        {
            ClientesService.ClientesServerClient client = new ClientesService.ClientesServerClient();

            var res = client.getUsuario(id);

            if (res != null)
            {
                Usuario usuario = new Usuario()
                {
                    id = res.id,
                    username = res.username,
                    nombre = res.nombre,
                    contrasena = res.contrasena,
                    tipoUsuario = res.tipoUsuario
                };
                return View(usuario);
            }
            return RedirectToAction("Index" , "Usuarios");
        }

        public ActionResult Agregar(int id, string name, string password, string username, string tipoUsuario)
        {
            ClientesService.ClientesServerClient client = new ClientesService.ClientesServerClient();
            client.insertUsuario(id, name, password, tipoUsuario, username);
            return RedirectToAction("Index", "Usuarios");
        }

        public ActionResult Eliminar(int id)
        {
            ClientesService.ClientesServerClient client = new ClientesService.ClientesServerClient();
            client.deleteUsuario(id);
            return RedirectToAction("Index", "Usuarios");
        }

        public ActionResult Actualizar(int id, string name, string password, string username, string tipoUsuario)
        {
            ClientesService.ClientesServerClient client = new ClientesService.ClientesServerClient();
            client.updateUsuario(id, name, password, tipoUsuario, username);
            return RedirectToAction("Index", "Usuarios");
        }

        public ActionResult Index()
        {
            ClientesService.ClientesServerClient client = new ClientesService.ClientesServerClient();

            List<ClientesService.usuario> listaUsuarios = client.getAllUsuarios().ToList();
            return View(listaUsuarios);
        }
    }
}