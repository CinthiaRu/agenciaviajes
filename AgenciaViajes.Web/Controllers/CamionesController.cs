﻿using AgenciaViajes.Web.Models;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgenciaViajes.Web.Controllers
{
    public class CamionesController : Controller
    {
        public ActionResult Origenes()
        {
            TransportesService.SWTransporteClient client = new TransportesService.SWTransporteClient();

            CamionOrigen camion = new CamionOrigen();
            Newtonsoft.Json.Linq.JArray listaOrigenes = (Newtonsoft.Json.Linq.JArray)JsonConvert.DeserializeObject(client.listaOrigenes());

            foreach (var origen in listaOrigenes)
            {
                OrigenJSON origen1 = JsonConvert.DeserializeObject<OrigenJSON>(origen.ToString());
                camion.Origen.Add(origen1.origenC);
            }

            return View(camion);
        }

        public ActionResult Destinos()
        {
            TransportesService.SWTransporteClient client = new TransportesService.SWTransporteClient();

            CamionDestino camion = new CamionDestino();
            Newtonsoft.Json.Linq.JArray listaDestinos = (Newtonsoft.Json.Linq.JArray)JsonConvert.DeserializeObject(client.listaDestinos());

            foreach (var destino in listaDestinos)
            {
                DestinoJSON destino1 = JsonConvert.DeserializeObject<DestinoJSON>(destino.ToString());
                camion.Destino.Add(destino1.destinoC);
            }
            return View(camion);
        }
        public ActionResult Reservar(string origen, string destino)
        {
            Corrida corrida = new Corrida()
            {
                Origen = origen,
                Destino = destino
            };
            return View(corrida);
        }
        public ActionResult BuscarCamion()
        {
            return View();
        }


        public ActionResult ComprarBoleto(string id, string origen, string destino, string fecha, string hora, int numAsientos, int precio, string cuenta)
        {
            TransportesService.SWTransporteClient client = new TransportesService.SWTransporteClient();
            var compraExitosa = client.comprarBoleto(id, origen, destino, fecha, hora, numAsientos, precio);
            if (compraExitosa)
            {
                if (!String.IsNullOrEmpty(cuenta))
                {
                    PagosService.PasarelaClient pago = new PagosService.PasarelaClient();
                    var res = pago.Transaccion(cuenta, "4595591987719047","200");
                    if (res != null)
                    {
                        return RedirectToAction("ConsultarTransaccion", "Pagos", new { idTransaccion = res });
                    }
                }
                return RedirectToAction("ConsultarTransaccion", "Pagos", new { idTransaccion = id });
            }
            else
                return RedirectToAction("Reservar", "Camiones");
        }

        public ActionResult Busqueda(string origen, string destino)
        {
            TransportesService.SWTransporteClient client = new TransportesService.SWTransporteClient();
            var listaRutas = client.buscarCamion(origen, destino);
            if (listaRutas == "[]")
                return RedirectToAction("BuscarCamion", "Camiones");
            else
                return RedirectToAction("Reservar", "Camiones", new { origen = origen, destino = destino });
        }
    }
}