﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace AgenciaViajes.Web.Controllers
{
    public class PagosController : Controller
    {
        public ActionResult Index(string correo, string nombre)
        {
            if(!String.IsNullOrEmpty(correo) && !String.IsNullOrEmpty(nombre))
            {
                PagosService.PasarelaClient client = new PagosService.PasarelaClient();
                var res = client.Registro(correo, nombre);

                if (res != null)
                {
                    ViewBag.clave = res;
                }
            }
            return View();
        }
        public ActionResult ConsultarHistorial(string cuenta)
        {
            if (!String.IsNullOrEmpty(cuenta)) {
                PagosService.PasarelaClient client = new PagosService.PasarelaClient();
                var res = client.ConsultarHistorial(cuenta);

                if (res != null)
                {
                    ViewBag.cuenta = res;
                }
            }
           
            return View();
        }
        public ActionResult ConsultarTransaccion(string id, string idTransaccion)
        {
            if (!String.IsNullOrEmpty(id))
            {
                PagosService.PasarelaClient client = new PagosService.PasarelaClient();
                var res = client.ConsultarTransaccion(id);

                if (res != null)
                {
                    ViewBag.transaccion = res;
                }
                
            }
            if (!String.IsNullOrEmpty(idTransaccion))
            {
                ViewBag.IdTransaccion = idTransaccion;
            }
            return View();
        }
    }
}