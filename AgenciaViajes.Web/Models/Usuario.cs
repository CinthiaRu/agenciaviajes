﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgenciaViajes.Web.Models
{
    public class Usuario
    {
        public int id { get; set; }
        public string username { get; set; }
        public string nombre { get; set; }
        public string contrasena { get; set; }
        public string tipoUsuario { get; set; }
    }
}