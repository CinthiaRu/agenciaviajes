﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgenciaViajes.Web.Models
{
    public class CamionDestino
    {
        public List<string> Destino { get; set; }
        public CamionDestino()
        {
           Destino = new List<string>();
        }
    }
}