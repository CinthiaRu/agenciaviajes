﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgenciaViajes.Web.Models
{
    public class CamionOrigen
    {
        public List<string> Origen { get; set; }
        public CamionOrigen()
        {
           Origen = new List<string>();
        }
    }
}