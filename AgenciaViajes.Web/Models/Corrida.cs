﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace AgenciaViajes.Web.Models
{
    public class Corrida
    {
        public string Origen { get; set; }
        public string Destino { get; set; }
    }
}